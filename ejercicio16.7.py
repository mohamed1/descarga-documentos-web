from urllib.request import urlopen

class Robot:
    def __init__(self, url):
        self.url = url
        self.document = None

    def retrieve(self):
        if self.document is None:
            print(f"Downloading... {self.url}...")
            with urlopen(self.url) as f:
                self.document = f.read().decode('utf-8')
            print('Url download')

    def show(self):
        self.retrieve()
        print(f'Content url: {self.document}')

    def content(self):
        self.retrieve()
        return self.document


class Cache:
    def __init__(self):
        self.documents = {}

    def retrieve(self, url):
        if url not in self.documents:
            self.documents[url] = Robot(url)
        self.documents[url].retrieve()

    def show(self, url):
        self.retrieve(url)
        print(self.documents[url].content())

    def show_all(self):
        for url in self.documents:
            print(url)

    def content(self, url):
        self.retrieve(url)
        return self.documents[url].content()


if __name__ == '__main__':
    print('-----EXERCISE 16.7-----')
    robot1 = Robot('https://www.python.org/')
    robot1.retrieve()
    robot1.content()
    robot1.show()
    robot2 = Robot('https://www.aulavirtual.urjc.es')
    robot2.retrieve()
    robot2.content()
    robot2.show()

    cache1 = Cache()
    cache1.retrieve('https://www.python.org/')
    cache1.content('https://www.python.org/')
    cache1.show('https://www.python.org/')
    cache1.show_all()

    cache2 = Cache()
    cache2.retrieve('https://www.aulavirtual.urjc.es')
    cache2.content('https://www.aulavirtual.urjc.es')
    cache2.show('https://www.aulavirtual.urjc.es')
    cache2.show_all()